#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <opencv2\highgui\highgui_c.h>
#include "view.h"
#include "linkedList.h"

//C:\\mag.png
//C:\\hey.png
//C:\\testing\\1.png

int main(void)
{
	int choice = 0;
	unsigned int duration = 0;
	int tmpNum = 0;
	char name[STR_LEN] = { 0 };
	FrameNode * head = NULL;
	do
	{
		printf("%s", MENU);
		scanf_s("%d", &choice);
		getchar();
		switch (choice)
		{
			case 0:
				break;
			case 1:
				insertAtEnd(&head, frameInput(&head));
				break;
			case 2:
				printf("Enter name to remove: \n");
				fgets(name, STR_LEN, stdin);

				if (searchFrame(&head, name))
				{
					deleteNode(&head, name);
				}
				else
				{
					printf("Frame not found!\n");
				}
				break;
			case 3:
				printf("Enter the name of the frame\n");
				fgets(name, STR_LEN, stdin);
				while (!searchFrame(&head, name))
				{
					printf("Frame was not found!\n");
					fgets(name, STR_LEN, stdin);
				}
				printf("Enter the new index you wish to place the frame\n");
				scanf_s("%d", &tmpNum);
				getchar();
				while (tmpNum > count(head))
				{
					printf("Invalid index!\n");
					scanf_s("%d", &tmpNum);
					getchar();
				}
				changePos(&head, name, tmpNum - 1);
				break;
			case 4:
				printf("Enter frame name to change its duration: \n");
				fgets(name, STR_LEN, stdin);

				if (searchFrame(&head, name))
				{
					printf("Enter the new duration of the frame: \n");
					scanf_s("%u", &duration);
					getchar();
					changeFrameDuration(&head, name, duration);
				}
				else
				{
					printf("Frame not found!\n");
				}
				break;
			case 5:
				printf("Enter the new duration of every frame: \n");
				scanf_s("%u", &duration);
				getchar();

				changeEveryFrameDuration(&head, duration);
				break;
			case 6:
				printf("		name		duration		path\n");
				printList(head);
				break;
			case 7:
				play(head);
			default:
				printf("Invalid choice!\n");
		}
	} while (choice != 0);

	printf("\nBye!");

	getchar();
	return 0;
}
