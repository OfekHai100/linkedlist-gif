#pragma once
/*********************************
* Class: MAGSHIMIM Final Project *
* Play function declaration	 	 *
**********************************/

#ifndef VIEWH
#define VIEWH

#include <opencv2\core\core_c.h>
#include <opencv2\highgui\highgui_c.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include "LinkedList.h"

#define GIF_REPEAT 5
#define MENU "What would you like to do?\n [0] - Exit\n [1] - Add new frame\n [2] - Remove a frame\n [3] - Change frame index\n [4] - Change frame duration\n [5] - Change duration of all\n [6] - List frames\n [7] - Play movie!\n [8] - Save project\n"
#define STR_LEN 30

void play(FrameNode *list);
void printList(FrameNode* list);
FrameNode* addFrameNode(Frame* frame);
FrameNode * frameInput(FrameNode** head);
void insertAtEnd(FrameNode** head, FrameNode* newNode);
void deleteNode(FrameNode** head, char* name);
void changeFrameDuration(FrameNode** head, char* searchName, unsigned int duration);
void changeEveryFrameDuration(FrameNode** head, unsigned int duration);
int searchFrame(FrameNode** head, char* searchName);
void changePos(FrameNode **head, const char *name, int pos);
int count(FrameNode *head);

#endif