/*********************************
* Class: MAGSHIMIM Final Project *
* Play function				 	 *
**********************************/

#include "view.h"

int cnt = 0;

/**
play the movie!!
display the images each for the duration of the frame one by one and close the window
input: a linked list of frames to display
output: none
**/
void play(FrameNode* list)
{
	cvNamedWindow("Display window", CV_WINDOW_AUTOSIZE); //create a window
	FrameNode* head = list;
	int imgNum = 1, playCount = 0;
	IplImage* image;
	char * pos;
	while (playCount < GIF_REPEAT)
	{
		while (list != 0)
		{
			if ((pos = strchr(&(list->frame->path), '\n')) != NULL)
			{
				*pos = '\0';
			}
			image = cvLoadImage(&(list->frame->path), 1);
			IplImage* pGrayImg = 0;
			pGrayImg = cvCreateImage(cvSize(image->width, image->height), image->depth, 1);
			if (!image) //The image is empty - shouldn't happen since we checked already.
			{
				printf("Could not open or find image number %d", imgNum);
			}
			else
			{
				cvShowImage("Display window", image); //display the image
				cvWaitKey(&(list->frame->duration)); //wait
				list = list->next;
				cvReleaseImage(&image);
			}
			imgNum++;
		}
		list = head; // rewind
		playCount++;
	}
	cvDestroyWindow("Display window");
	return;
}

//************************************
// Method:    printList
// Returns:   void
// Description: prints list recursively
// Parameter: FrameNode * list
// Runtime: O(length of list)
//************************************
void printList(FrameNode* list)
{
	if (list)
	{
		printf("Frame name: %s", &(list->frame->name));
		printf("Frame duration: %u ms\n", list->frame->duration);
		printf("Frame path: %s\n", &(list->frame->path));
		printList(list->next);
	}
	else
	{
		printf("\n");
	}
}

/**
Function will create a frame node
input:
the frame
output:
the frame node updated with the frame
*/
FrameNode* addFrameNode(Frame* frame)
{
	FrameNode* newFrameNode = (FrameNode*)malloc(sizeof(FrameNode));

	newFrameNode->frame = frame;
	newFrameNode->next = NULL;

	return newFrameNode;
}

/**
Function will get user's input to the frame that inputed in the frameNode function and it will return a frame node.
input:
the head frame node
output:
the frame node updated with the frame
*/
FrameNode * frameInput(FrameNode** head)
{
	Frame* newFrame = (Frame*)malloc(sizeof(Frame));
	
	printf("*** Creating new frame ***\n");
	printf("Please insert frame path:\n");
	do
	{
		fgets(&(newFrame->path), STR_LEN, stdin);

	} while (strlen(&(newFrame->path)) == 1);

	printf("Please insert frame duration(in miliseconds):\n");
	scanf("%u", &newFrame->duration);
	getchar();

	printf("Please choose a name for that frame:\n");
	do
	{
		fgets(&(newFrame->name), STR_LEN, stdin);
	} while (strlen(&(newFrame->name)) == 1);
	

	while (cnt != 0 && searchFrame(&(*head), &(newFrame->name)))
	{
		printf("The name is already taken, please enter another name\n");
		fgets(&(newFrame->name), STR_LEN, stdin);
	}
	cnt++;

	return addFrameNode(newFrame);
}



/**
Function will add a frame node to the list
input:
head - the first node of the list
newNode - the new frame node to add to the list
output:
none
*/
void insertAtEnd(FrameNode** head, FrameNode* newNode)
{
	FrameNode* curr = *head;

	if (!*head) // empty list!
	{
		*head = newNode;
	}
	else
	{
		while (curr->next) // while the next is NOT NULL (when next is NULL - that is the last node)
		{
			curr = curr->next;
		}

		curr->next = newNode;
		newNode->next = NULL;
	}
}

/**
Function will delete a specific frame node from a list of frame nodes
input:
the list (the first frame node)
name - the frame node to delete
output:
none
*/
void deleteNode(FrameNode** head, char* name)
{
	FrameNode* curr = *head;
	FrameNode* temp = NULL;

	// if the list is not empty (if list is empty - nothing to delete!)
	if (*head)
	{
		// the first node should be deleted?
		if (0 == strcmp(&((*head)->frame->name), name))
		{
			*head = (*head)->next;
			free(curr);
		}
		else
		{
			while (curr->next)
			{
				if ((0 == strcmp(&(curr->next->frame->name), name))) // waiting to be on the node BEFORE the one we want to delete
				{
					temp = curr->next; // put aside the node to delete
					curr->next = temp->next; // link the node before it, to the node after it
					free(temp); // delete the node
				}
				else
				{
					curr = curr->next;
				}
			}
		}
	}
}

/**
Function will search a specific person from a list of persons and return if he was found or not.
input:
head - the list (the first frame node)
searchName - the name of the frame node to search
output:
if the frame node was found or not (TRUE or FALSE)
*/
int searchFrame(FrameNode** head, char* searchName)
{
	FrameNode* current = *head;  // Initialize current 

	while (current != NULL)
	{
		if (strcmp(&(current->frame->name), searchName) == 0)
		{
			return TRUE;
		}
		current = current->next;
	}
	return FALSE;
}

/**
Function will change the duration of a frame node entered by the user to the duration he entered (recursively).
input:
head - the list (the first frame node)
searchName - the name of the frame node to change duration
duration - the new duration of the node
output:
none
*/
void changeFrameDuration(FrameNode** head, char* searchName, unsigned int duration)
{
	FrameNode* curr = *head;

	if (strcmp(&(curr->frame->name), searchName) == 0)
	{
		curr->frame->duration = duration;
	}
	else
	{
		changeFrameDuration(&(curr->next), searchName, duration);
	}
}


/**
Function will change the duration of every frame node to the duration entered by the user (recursively).
input:
head - the list (the first frame node)
searchName - the name of the frame node to change duration
duration - the new duration of the node
output:
none
*/
void changeEveryFrameDuration(FrameNode** head, unsigned int duration)
{
	FrameNode* curr = *head;

	if (curr != NULL)
	{
		curr->frame->duration = duration;
		changeEveryFrameDuration(&(curr->next), duration);
	}
}

/**
Function will change the position of the frame node entered by the user to a new position in the list entered by the user.
input:
head - the list (the first frame node)
name - the name of the frame node to change its postion
pos - the new position of the frame node
output:
none
*/
void changePos(FrameNode **head, const char *name, int pos)
{
	assert(head != 0 && name != 0 && pos >= 0);
	FrameNode *root = *head;
	FrameNode *link = root;
	FrameNode *prev = 0;
	int count = 0;
	while (link != 0 && strcmp(&(link->frame->name), name) != 0)
	{
		prev = link;
		link = link->next;
		count++;
	}
	if (link == 0)      // Name not found - no swap!
	{
		return;
	}
	if (count == pos)   // Already in target position - no swap
	{
		return;
	}
	if (count == 0)     // Moving first item; update root
	{
		assert(link == root);
		*head = root->next;
		root = *head;
	}
	else
	{
		assert(prev != 0);
		prev->next = link->next;
	}
	// link is detached; now where does it go?
	if (pos == 0)       // Move to start; update root
	{
		link->next = root;
		*head = link;
		return;
	}
	FrameNode *node = root;
	for (int i = 0; i < pos - 1 && node->next != 0; i++)
	{
		node = node->next;
	}
	link->next = node->next;
	node->next = link;
}

/**
Function will count a list of frame nodes (recursively).
input: the list (the first frame node)
output:
the number of frame nodes in the list
*/
int count(FrameNode *head)
{
	// Base case 
	if (head == NULL)
	{
		return 0;
	}

	// count is 1 + count of remaining list 
	return 1 + count(head->next);
}